<?php
	/*
	Plugin Name: String overrides
	Description: Easy override strings, that otherwise aren't easily translateable.
	Author: SharksMedia ApS
	Author URI: http://sharksmedia.dk
	Author Email: info@sharksmedia.dk
	Version: 1.0.0
	License: WTFPL
	Text Domain: sharksmedia
	*/
	defined("ABSPATH") or die("Kiddie free zone!");

	function stringoverrides_add_message($message, $type = "error") {
		add_action( 'admin_notices', function() use ($message, $type) {
			?>
				<div class="<?php print $type; ?>"><p><?php print __( $message, "sharksmedia" ); ?></p></div>
			<?php
		} );
	}

	add_action( "plugins_loaded", function() {
		$jsonfile = dirname(__FILE__) . "/stringoverrides.json";

		if( !file_exists( $jsonfile ) ) {
			stringoverrides_add_message( "The stringoverrides.json file does not exist, it must be created in: ".$jsonfile );
		} else {
			$overrides = json_decode( file_get_contents( $jsonfile ) );

			if( json_last_error() !== JSON_ERROR_NONE ) {
				stringoverrides_add_message( "The stringoverrides.json is improperly configured (" . json_last_error_msg() . ")" );	
			} else {
				add_filter( "gettext", function($translated_text, $text, $textdomain ) use ($overrides) {
					
					if(isset($overrides->$textdomain->$text)) {
						$translated_text = $overrides->$textdomain->$text;
					}

					return $translated_text;
				}, 20, 3 );
			}
		}

	} );