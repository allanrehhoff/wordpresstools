<?php
	/**
	* The prefix string to be prepended all uri routes
	* Alias, first part of the url, that directs any proceeding args to this plugin
	* @return string
	*/
	function cp_page_slug() {
		return "tilmelding";
	}

	/**
	* Translates filenames to a URI.
	* key = The filename of the template to be used.
	* value = The URI part
	* @return array A filename -> route map
	*/
	function cp_get_routes() {
		return [
			// "course-registration" => "start-tilmelding"
		];
	}

	/**
	* Get link to specified page.
	* @param $path - Member page slug to get link for
	* @param $args - Additional query args to be appended the url
	*/
	function cp_get_link( $path = '', $args = [] ) {
		$link = '/' . cp_page_slug();

		$routes = cp_get_routes();

		if( trim( $path ) != '' ) {
			if( isset( $routes[$path] ) ) {
				$path = $routes[$path];
			}

			$link .= '/' . $path;
		}

		if( ! empty( $args ) ) {
			$link .= '/?' . http_build_query( $args );
		}

		return $link;
	}

	/**
	 * Locate template.
	 *
	 * Locate the called template.
	 * Search Order:
	 * 1. /themes/theme/loop-api-templates/$template_name
	 * 2. /themes/theme/$template_name
	 * 3. /plugins/loop-api-integration/templates/$template_name.
	 *
	 * @param 	string 	$template_name			Template to load.
	 * @param 	string 	$string $template_path	Path to templates.
	 * @param 	string	$default_path			Default path to template files.
	 * @return 	string 							Path to the template file.
	 */
	function cp_locate_template( $template_name, $template_path = "", $default_path = "" ) {
		// Append .php to the template name, if it wasn't provided as argument.
		if( pathinfo( $template_name, PATHINFO_EXTENSION ) != "php" ) {
			$template_name .= ".php";
		}

		// Set variable to search in cp-templates folder of theme.
		if ( ! $template_path ) {
			$template_path = "cp-templates/";
		}

		// Set default plugin templates path.
		if ( ! $default_path ) {
			$default_path = plugin_dir_path( cp_PLUGIN_DIR . "/custom-plugin" ) . "templates/"; // Path to the template folder
		}

		// Search template file in theme folder.
		$template = locate_template( [
			$template_path . $template_name,
			$template_name
		] );

		// Get plugins template file.
		if ( ! $template ) {
			$template = $default_path . $template_name;
		}

		return apply_filters( __FUNCTION__, $template, $template_name, $template_path, $default_path );
	}

	/**
	* Parse url args, and return one or more values
	* @return string
	*/
	function cp_arg( $index = null ) {
		static $args;

		if( empty( $args ) ) {
			$args = explode( '/', parse_url( $_SERVER["REQUEST_URI"],  PHP_URL_PATH ) );
			$args = array_values( array_filter( $args ) );
		}

		if( $index == null ) {
			return $args;
		}

		return $args[$index] ?? null;
	}

	/**
	* Determines if the current url belongs to this plugin
	* @return mixed
	*/
	function is_cp_page() {
		$args = cp_arg();
		return ! empty( $args ) && $args[0] == cp_page_slug();
	}

	/**
	* Wrapper/helper function used in templates to set
	* the title for each individual page.
	* @return void
	*/
	function cp_set_title( $title ) {
		add_filter( "document_title_parts", function( $title_parts ) use ( $title ) {
			$title_parts["title"] = $title;
			return $title_parts;
		});
	}

	/**
	* Validates a date time representation according to a given format.
	*
	* @param $datetime - The date time string representation
	* @param $format - The format which $datetime should validate against.
	* @return bool
	*/
	function cp_validate_datetime( $datetime, $format = "Y-m-d H:i:s" ) {
		$d = DateTime::createFromFormat( $format, $datetime );
		return $d && $d->format( $format ) === $datetime;
	}
