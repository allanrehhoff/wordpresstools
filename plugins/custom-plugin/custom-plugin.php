<?php
	/**
	 * Plugin Name: ATR Custom Plugin
	 * Description: Custom made changes
	 * Version:     1.0.0
	 * Author:      Allan Thue Rehhoff
	 * Author URI:  https://rehhoff.me
	 * Text Domain: atr
	 */

	/**
	* Define current plugin version for use in enqueued assets
	*/
	add_action( "plugins_loaded", function() {
		if( !function_exists("get_plugin_data") ) { require_once( ABSPATH . "wp-admin/includes/plugin.php" );

		define( "CP_PLUGIN_VERSION", get_plugin_data( __FILE__ )["Version"] );
		define( "CP_PLUGIN_DIR", plugin_dir_path( __FILE__  ) );
		define( "CP_PLUGIN_URL", plugin_dir_url( __FILE__ ) );

		require CC_PLUGIN_DIR . "include/functions.php";
	} );

	/**
	* Enqueue custom assets into wp-admin
	*/
	add_action( "admin_enqueue_scripts", function() {
		wp_enqueue_script( "sharksmedia-custom-script", plugins_url( "assets/js/sharksmedia-plugin.js", __FILE__ ), ["jquery"], CP_PLUGIN_VERSION, true );
		wp_enqueue_style( "sharksmedia-custom-style", plugins_url( "assets/css/sharksmedia-plugin.css", __FILE__ ), false, CP_PLUGIN_VERSION );
	} );

	/**
	 * Template loader/router.
	 *
	 * The router will check if the url contains
	 * a structure belonging to custom member templates
	 *
	 * Reverse lookups the passed URI in our routing table,
	 * to see which template file should be used.
	 *
	 * @since 1.0.0
	 *
	 * @param	string	$template	Template file that is being loaded.
	 * @return	string				Template file that should be loaded.
	 */
	add_filter( "template_include", function( $template ) {
		if ( is_cc_page() ) {
			$routes = cp_get_routes();

			$args = cc_arg();
			$page = $args[1] ?? "login";

			if( in_array( $page, $routes ) ) {
				$template_name = array_search( $page, $routes );
			} else {
				$template_name = $page;
			}

			$location = cp_locate_template( $template_name );

			if ( file_exists( $location ) ) {
				$GLOBALS["wp_query"]->is_404 = false;

				status_header( 200 );
				cp_set_title( ucfirst( $page ) );

				add_filter( "body_class", function( $classes ) use ( $template_name ) {
					return [ "cc-page", "cc-page-" . $template_name ];
				} );

				$template = $location;
			}
		}

		return $template;
	}, 1 );
