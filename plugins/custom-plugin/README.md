# Allan Rehhoff Custom Plugin #

Boilerplate for quickstarting a custom wordpress plugin.  
Use this for various enchancements or changes you want applied to the backend.  

Can also be used to hook into template themes functionality.  

Don't forget to bump the plugin version number, whenever changes are made.  
Read the standards for semantic versioning: https://semver.org/