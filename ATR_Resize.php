<?php
/**
 * Title         : ATR Resizer
 * Description   : Resizes WordPress images on the fly, caches files as they are resized.
 *
 * @param string  $url      - (required) must be uploaded using wp media uploader
 * @param int     $width    - (required)
 * @param int     $height   - (optional)
 * @param bool    $crop     - (optional) default to soft crop
 * @param bool    $single   - (optional) returns an array if false
 * @param bool    $upscale  - (optional) resizes smaller images
 * @uses wp_upload_dir()
 * @uses image_resize_dimensions()
 * @uses wp_get_image_editor()
 * @uses wp_parse_args()
 * @throws ATR_ResizeException
 * @version 1.0
 * @author Allan Thue Rehhoff
 * @return str|object
 */

if(!class_exists("ATR_Resize")) {
	class ATR_ResizeException extends Exception {}

	class ATR_Resize {
		/**
		 * The singleton instance
		 */
		static private $instance = null;

		/**
		 * Should an ATR_ResizeException be thrown on error?
		 * If false (default), then the error will just be logged.
		 */
		public $throwOnError = false;

		/**
		 * No initialization allowed
		 */
		private function __construct() {}

		/**
		 * No cloning allowed
		 */
		private function __clone() {}

		/**
		 * For your custom default usage you may want to initialize an ATR_Resize object by yourself and then have own defaults
		 */
		static public function getInstance() {
			if(self::$instance == null) {
				self::$instance = new self;
			}

			return self::$instance;
		}

		/**
		 * Run, forest.
		 */
		public function process( $url, $args = array()) {
			try {
				// $width = null, $height = null, $crop = null, $upscale = false, $single = true
				$defaults = array(
					"width" => null,
					"height" => null,
					"crop" => null,
					"upscale" => null,
					"single" => true
				);

				// Ugh., if anyone asks about this particular line,
				// I did not write it... 
				extract(wp_parse_args($args, $defaults)); 

				// Validate inputs.
				if ($url === null) {
					throw new ATR_ResizeException("url parameter is required");
				}
				if ($width === null) {
					throw new ATR_ResizeException("width parameter is required");
				}
				if ($height === null) {
					throw new ATR_ResizeException("height parameter is required");
				}

				// Super nasty, WPML Fix, but sadly this one was out of my hands...
				if ( defined( 'ICL_SITEPRESS_VERSION' ) ){
					global $sitepress;
					$url = $sitepress->convert_url( $url, $sitepress->get_default_language() );
				}

				// Caipt'n, ready to hook.
				if ( true === $upscale ) {
					add_filter( "image_resize_dimensions", array($this, "atr_upscale"), 10, 6 );
				}

				// Define upload path & dir.
				$upload_info = wp_upload_dir();
				$upload_dir = $upload_info["basedir"];
				$upload_url = $upload_info["baseurl"];
				
				$http_prefix = "http://";
				$https_prefix = "https://";
				$relative_prefix = "//"; // The protocol-relative URL
				
				/*
				* if the $url scheme differs from $upload_url scheme, make them match 
				* if the schemes differe, images don't show up.
				*/
				if(!strncmp($url,$https_prefix, mb_strlen($https_prefix))) {
					//if url begins with https:// make $upload_url begin with https:// as well
					$upload_url = str_replace($http_prefix,$https_prefix,$upload_url);
				} elseif(!strncmp($url,$http_prefix, mb_strlen($http_prefix))) {
					//if url begins with http:// make $upload_url begin with http:// as well
					$upload_url = str_replace($https_prefix,$http_prefix,$upload_url);      
				} elseif(!strncmp($url,$relative_prefix, mb_strlen($relative_prefix))) {
					//if url begins with // make $upload_url begin with // as well
					$upload_url = str_replace(array( 0 => $http_prefix, 1 => $https_prefix), $relative_prefix,$upload_url);
				}
				

				// Check if $img_url is local.
				if ( false === strpos( $url, $upload_url ) ) {
					throw new ATR_ResizeException("Image cannot be located externally: " . $url);
				}

				// Define path of image.
				$rel_path = str_replace( $upload_url, '', $url );
				$img_path = $upload_dir . $rel_path;

				// Check if img path exists, and is an image indeed.
				if ( ! file_exists( $img_path ) or ! getimagesize( $img_path ) ) {
					throw new ATR_ResizeException("Image file does not exist (or is not an image): " . $img_path);
				}

				// Get image info.
				$info = pathinfo( $img_path );
				$ext = $info["extension"];
				list( $orig_w, $orig_h ) = getimagesize( $img_path );

				// Get image size after cropping.
				$dims = image_resize_dimensions( $orig_w, $orig_h, $width, $height, $crop );
				$dst_w = $dims[4];
				$dst_h = $dims[5];

				// Return the original image only if it exactly fits the needed measures.
				if ( ! $dims && ( ( ( null === $height && $orig_w == $width ) xor ( null === $width && $orig_h == $height ) ) xor ( $height == $orig_h && $width == $orig_w ) ) ) {
					$img_url = $url;
					$dst_w = $orig_w;
					$dst_h = $orig_h;
				} else {
					// Use this to check if cropped image already exists, so we can return that instead.
					$suffix = $dst_w.'x'.$dst_h;
					$dst_rel_path = str_replace( '.' . $ext, '', $rel_path );
					$destfilename = $upload_dir.$dst_rel_path.'-'.$suffix.'.'.$ext;

					if ( ! $dims || ( true == $crop && false == $upscale && ( $dst_w < $width || $dst_h < $height ) ) ) {
						// Can't resize, so return false saying that the action to do could not be processed as planned.
						throw new ATR_ResizeException('Unable to resize image because image_resize_dimensions() failed');
					} elseif ( file_exists( $destfilename ) && getimagesize( $destfilename ) ) {
						// Perhaps a cache exists.? No need to resize if it already exists.
						$img_url = $upload_url.$dst_rel_path.'-'.$suffix.'.'.$ext;
					} else {
						// Finally resize the image and return whatever the client requested.
						$editor = wp_get_image_editor( $img_path );
						$resize = $editor->resize( $width, $height, $crop );

						if ( is_wp_error( $editor ) || is_wp_error( $resize ) ) {
							throw new ATR_ResizeException("Unable to get WP_Image_Editor: " . $editor->get_error_message() . " (requires GD or ImageMagick extension)");
						}

						$resized_file = $editor->save();

						if ( ! is_wp_error( $resized_file ) ) {
							$resized_rel_path = str_replace( $upload_dir, '', $resized_file["path"] );
							$img_url = $upload_url . $resized_rel_path;
						} else {
							throw new ATR_ResizeException("Unable to save resized image file: " . $editor->get_error_message());
						}
					}
				}

				// Okay, leave the ship.
				if ( true === $upscale ) {
					remove_filter( "image_resize_dimensions", array( $this, "atr_upscale" ) );
				}

				// Return the output.
				if ( $single === true) {
					$image = $img_url;
				} else {
					$image = (object) array(
						"url" => $img_url,
						"width" => $dst_w,
						"height" => $dst_h
					);
				}

				return $image;
			} catch (ATR_ResizeException $ex) {
				error_log('ATR_Resize.process() error: ' . $ex->getMessage());

				if ($this->throwOnError) {
					throw $ex;
				} else {
					return false;
				}
			}
		}

		/**
		 * Callback to overwrite WP computing of thumbnail measures
		 */
		function atr_upscale( $default, $orig_w, $orig_h, $dest_w, $dest_h, $crop ) {
			if ( ! $crop ) return null; // Let the wordpress default function handle this.

			// Here is the point we allow to use larger image size than the original one.
			$aspect_ratio = $orig_w / $orig_h;
			$new_w = $dest_w;
			$new_h = $dest_h;

			if ( ! $new_w ) {
				$new_w = intval( $new_h * $aspect_ratio );
			}

			if ( ! $new_h ) {
				$new_h = intval( $new_w / $aspect_ratio );
			}

			$size_ratio = max( $new_w / $orig_w, $new_h / $orig_h );

			$crop_w = round( $new_w / $size_ratio );
			$crop_h = round( $new_h / $size_ratio );

			$s_x = floor( ( $orig_w - $crop_w ) / 2 );
			$s_y = floor( ( $orig_h - $crop_h ) / 2 );

			return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
		}
	}
}

if(!function_exists("atr_resize")) {
	/**
	 * This is just a tiny wrapper function for the class above so that there is no
	 * need to change any code in your own WP themes. Usage is still the same :)
	 */
	function atr_resize( $url, $args) {
		$atr_resize = ATR_Resize::getInstance();
		return $atr_resize->process( $url, $args );
	}
}

if(!function_exists("atr_resize_thumbnail")) {
	function atr_resize_thumbnail($postid, $args) {
		$thumb = wp_get_attachment_url(get_post_thumbnail_id($article->ID), 'full');
		$image = atr_resize( $thumb, $args );
		return $image;
	}
}
