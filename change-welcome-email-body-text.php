<?php
/**
 * Plugin Name: Change Welcome Email Body Text
 * Description: Defined as a (must-use) in order to prevent normal plugins from taking precedence.
 * Version: 1.0.0
 * Author: SharksMedia ApS
 * Author URI: http://sharksmedia.dk/
 * Author Email: allan@sharksmedia.dk
 */

/**
* Rebuilds and resets a user's activation link.
* @author Allan Rehhoff
* @param $user A wordpress user object, must be an instsance of WP_User.
* @return (string)
*/
function rebuild_user_activation_link(WP_User $user) {
		global $wpdb, $wp_hasher;

		// Generate something random for a password reset key, used for getting and returning an activation link.
		$key = wp_generate_password( 20, false );

		// This action is documented in wp-login.php
		do_action( 'retrieve_password_key', $user->user_login, $key );

		// Now insert the key, hashed, into the DB.
		if ( empty( $wp_hasher ) ) {
			$wp_hasher = new PasswordHash( 8, true );
		}

		$hashed = time() . ':' . $wp_hasher->HashPassword( $key );
		$wpdb->update( $wpdb->users, [ 'user_activation_key' => $hashed ], [ 'user_login' => $user->user_login ] );

		return network_site_url("wp-login.php?action=rp&key=".$key."&login=" . rawurlencode($user->user_login), 'login');
}

/**
* Redefine user notification function See pluggable.php
* @author Allan Rehhoff
* @param $user_id A wordpress user ID
* @param $plaintext_pass The user defined password in plaintext
* @return void
*/
if ( !function_exists('wp_new_user_notification') ) {
	function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
		global $wpdb;
		$user = new WP_User($user_id);
 
		$user_login = stripslashes($user->user_login);
		$user_email = stripslashes($user->user_email);

		/** Generate subsriptions list */
		$subscription_list_html = '';
		if(!empty($_REQUEST["subscribr-terms"])) {
			$terms = $wpdb->get_col( "SELECT `name` FROM ".$wpdb->prefix."terms WHERE `slug` IN ('".implode("', '", esc_sql($_REQUEST["subscribr-terms"]))."')" );
			$subscription_list_html .= "<ul><li>".implode("</li><li>", $terms)."</li></ul>";
		}

		$message = '
			<h1>Tak for din tilmelding.</h1>
			<br>
			Du er nu tilmeldt følgende emner og vil fremadrettet modtage en email hver gang der kommer et nyt indlæg på det du her valgt.<br>
			<br>
			@subscriptions<br>
			<br>
			Ønsker du at redigere dine indstillinger så kan du klikke <b><u><a href="@activationlink">her</a></u></b> og oprette et kodeord for at få adgang til din profil.
		';

  		$replacements = [
  			"@subscriptions" => $subscription_list_html,
  			"@activationlink" => rebuild_user_activation_link($user)
  		];

		$message = str_replace(array_keys($replacements), array_values($replacements), $message);
		wp_mail($user_email, sprintf('[%s] Tak for din tilmelding.', get_option('blogname')), $message);
	}
}

/**
* Change user already registered error
* @param $errors an array of errors already provided.
* @param $sanitized_user_login User's username after it has been sanitized.
* @param $user_email User's email.
* @author Allan Rehhoff
*/
add_filter( 'registration_errors', function($errors, $sanitized_user_login, $user_email) {
	if(isset($errors->errors['email_exists'])) {
		$errors->errors["email_exists"][0] = "Din e-mail adresse er allerede registreret, ønsker du  istedet at <a href='".wp_lostpassword_url()."'>ændre dine notifikationer</a>?";
	}

	return $errors;
}, 10, 3 );