<?php
    /*
    * @Author Allan Thue Rehhoff
    */

    require 'wp-blog-header.php';
    require 'wp-includes/registration.php';

    function randomPassword($length = 16) {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';
        $max = strlen($chars) - 1;

        $str = '';
        for ($i=0; $i < $length; $i++) {
            $str .= $chars[mt_rand(0, $max)];
        }

        return $str;
    }

    $change_admin_name_to = 'sharksmedia';

    $uid = isset($_GET['uid']) ? $_GET['uid'] : 1;
    $newusername = isset($_GET['username']) ? $_GET['username'] : $change_admin_name_to;
    $user_info = get_userdata($uid);
    $username_changed = false;
    $newpassword = randomPassword();

    if(strtolower($user_info->user_login) == 'admin') {
        $user_info->user_login = $newusername;
        $username_changed = true;
    }

    $user_info->user_pass = $newpassword;

    wp_update_user($user_info);

    if(!unlink(__FILE__)) {
        print "<h1 style='color:red;'>This script could not be deleted.. Please delete immediately!</h2>";
    } else {
        print "<h1>Done... This file has been deleted automatically!</h1>";
    }

    if($username_changed === true) {
        print "<div style='color:rgb(0, 122, 255);'>Username was changed from 'admin' to '".$change_admin_name_to."' </div>";
    }

    print '<div>ID: '.$user_info->ID.'</div>';
    print '<div>Email: '.$user_info->user_email.'</div>';
    print '<div>Username: '.$user_info->user_login.'</div>';
    print '<div>Password: '.$newpassword.'</div>';
    print ' <br>
            <form name="loginform" action='.wp_login_url().' method="POST">
                <input type="text" name="log" value="'.$user_info->user_login.'" />
                <input type="password" name="pwd" value="'.$newpassword.'" />
                <input type="submit" value="Log in" name="wp-submit" />
            </form>';
?>