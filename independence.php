<?php
/*
	// does the current post id belong to a translation which is a duplicate?
	function icl_is_translated_duplicate(){
	       
	    $icl_lang_duplicate_of      = get_post_meta( get_the_ID(), '_icl_lang_duplicate_of', TRUE );
	    $is_duplicate               = (!empty($icl_lang_duplicate_of) ? TRUE : FALSE );
	       
	return $is_duplicate;
	}
	 
	$end = icl_is_translated_duplicate();
	var_dump($end);
*/
	/**
	* Forces all duplicated WPML posts to be "Translated independently."
	*/
	include('wp-load.php');
	 
	global $wpdb;
	 
	$sql = "DELETE FROM {$wpdb->postmeta} WHERE meta_key = '_icl_lang_duplicate_of'";
	$wpdb->query($sql);
	 
	echo 'DONE';

	unlink(__FILE__);