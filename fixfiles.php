<?php
	$_SERVER["HTTP_HOST"] = '127.0.0.1';
	require "wp-load.php";

	$files = [];
	$path = "wp-content/uploads/";

	$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
	foreach($iterator as $fileObject) {
		if($fileObject->getFilename() == '.' || $fileObject->getFilename() == "..") continue;

		$files[] = $fileObject->getPathname();
	}

	$charMap = [
		"Ã¦" => 'ae',
		"Ã¸" => 'oe',
		"aÌ" => 'aa',
		"Ã" => 'OE',
		"Ã" => 'OE',
		'Ø' => 'OE',
		'æ' => 'ae',
		'ø' => 'oe',
		'å' => 'aa',
		'å' => 'aa',
		' ' => '-',
	];

	$charMapKeys = array_keys($charMap);
	$charMapValues = array_values($charMap);

	$regex = "/[^A-Za-z0-9_ \.\-\/]/";

	foreach($files as $key => $file) {
		$fixedFilename = preg_replace($regex, '', str_replace($charMapKeys, $charMapValues, $file) ); //str_replace($charMapKeys, $charMapValues, $file);
		//rename($file, $fixedFilename);
		print "FILE: ".$file." => ".$fixedFilename."<br>";
		//$files[$key] = $fixedFilename;
	}

	global $wpdb;
	$in =  [
		"_wp_attached_file",
		"_wp_attachment_metadata",
	];
	$fileRows = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key IN ('".implode("', '", $in)."')");

	foreach($fileRows as $file) {
		$newFilename = preg_replace($regex, '', str_replace($charMapKeys, $charMapValues, $file->meta_value) );
		//$wpdb->update($wpdb->prefix."postmeta", ["meta_value" => $newFilename], ["meta_id" => $file->meta_id]);
		print "DB: ".$file->meta_value." => ".$newFilename."<br>";
	}
/*
	print "<pre>";
	print_r($files);
	print "<pre>";
*/
?>