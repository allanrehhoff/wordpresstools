<?php
	require 'wp-blog-header.php';
	require 'wp-includes/registration.php';

	function randomPassword($length = 16) {
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';
		$max = strlen($chars) - 1;

		$str = '';
		for ($i=0; $i < $length; $i++) {
			$str .= $chars[mt_rand(0, $max)];
		}

		return $str;
	}

	$newusername = isset($_GET["username"]) ? $_GET["username"] : 'sharksmedia';
	$newpassword = randomPassword();
	$newemail = isset($_GET["email"]) ? $_GET["email"] : 'info@sharksmedia.com';

	// Check that user doesn't already exist
	if ( !username_exists($newusername) && !email_exists($newemail) ) {
		// Create user and set role to administrator
		$user_id = wp_create_user( $newusername, $newpassword, $newemail);
		if (is_int($user_id)) {
			$wp_user_object = new WP_User($user_id);
			$wp_user_object->set_role('administrator');

			echo '<h1>Successfully created new admin user.</h1>';
			echo 'Username: '.$newusername.'<br>';
			echo 'Password: '.$newpassword.'<br>';
			echo '	<br>
					<form name="loginform" action='.wp_login_url().' method="POST">
						<input type="text" name="log" value="'.$newusername.'" />
						<input type="password" name="pwd" value="'.$newpassword.'" />
						<input type="submit" value="Log in" name="wp-submit" />
					</form>';

			update_user_meta($user_id, "first_name", "SharksMedia");
			update_user_meta($user_id, "last_name", "ApS");

			if(!unlink(__FILE__)) {
				echo '<div style="color:red;">This script could not be automaitcally removed!<br>Please remove manually.';
			}
		} else {
			echo 'Error with wp_insert_user. No users were created.';
		}
	} else {
		echo 'This user or email already exists. Nothing was done.';
	}